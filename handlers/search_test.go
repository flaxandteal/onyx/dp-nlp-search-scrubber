package handlers

import (
	"os"
	"testing"

	"github.com/alediaferia/prefixmap"
	"gitlab.com/flaxandteal/onyx/dp-nlp-search-scrubber/db"
	"gitlab.com/flaxandteal/onyx/dp-nlp-search-scrubber/payloads"
)

func TestGetAllMatchingIndustries(t *testing.T) {
	// Split API tests from Unit tests
	skipUnitTests(t)
	// get a mock ScrubberDB with some industries
	mockDB := mockDb()

	tests := []struct {
		name          string
		query         []string
		expectedCodes []string
	}{
		{
			name:          "matching single query",
			query:         []string{"ind1"},
			expectedCodes: []string{"IND1"},
		},
		{
			name:          "matching multiple queries",
			query:         []string{"ind1", "ind2"},
			expectedCodes: []string{"IND1", "IND2"},
		},
		{
			name:          "no matching queries",
			query:         []string{"foo", "bar"},
			expectedCodes: []string{},
		},
		// algorithm of PrefixMap is depth first search
		// so It will get the data in reverse
		// keep that in mind when updating tests
		{
			name:          "matching partial query",
			query:         []string{"ind"},
			expectedCodes: []string{"IND3", "IND2", "IND1"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			matchingIndustries := getAllMatchingIndustries(tt.query, mockDB)
			if len(matchingIndustries) != len(tt.expectedCodes) {
				t.Errorf("expected %d matching industries, got %d", len(tt.expectedCodes), len(matchingIndustries))
				return
			}
			for i, industryResp := range matchingIndustries {
				if industryResp.Code != tt.expectedCodes[i] {
					t.Errorf("expected industry with code %s, got %s", tt.expectedCodes[i], industryResp.Code)
				}
			}
		})
	}
}

func TestGetAllMatchingAreas(t *testing.T) {
	// Split API tests from Unit tests
	skipUnitTests(t)
	// get a mock ScrubberDB with some areas
	mockDB := mockDb()
	tests := []struct {
		name          string
		query         []string
		expectedNames []*payloads.AreaResp
	}{
		{
			name:  "matching single query",
			query: []string{"OAC1"},
			expectedNames: []*payloads.AreaResp{
				{
					Name:       "LAN1",
					Region:     "RN1",
					RegionCode: "RC1",
					Codes: map[string]string{
						"OAC1": "OAC1",
					},
				},
			},
		},
		{
			name:  "matching multiple queries",
			query: []string{"OAC1", "OAC2"},
			expectedNames: []*payloads.AreaResp{
				{
					Name:       "LAN1",
					Region:     "RN1",
					RegionCode: "RC1",
					Codes: map[string]string{
						"OAC1": "OAC1",
					},
				},
				{
					Name:       "LAN2",
					Region:     "RN2",
					RegionCode: "RC2",
					Codes: map[string]string{
						"OAC2": "OAC2",
					},
				},
			},
		},
		{
			// PrefixMap algorithm is depth first search
			// so when running partial queries it will get
			// the last area first, keep that in mind when updating tests
			name:  "matching partial queries",
			query: []string{"OAC"},
			expectedNames: []*payloads.AreaResp{
				{
					Name:       "LAN3",
					Region:     "RN3",
					RegionCode: "RC3",
					Codes: map[string]string{
						"OAC3": "OAC3",
					},
				},
				{
					Name:       "LAN2",
					Region:     "RN2",
					RegionCode: "RC2",
					Codes: map[string]string{
						"OAC2": "OAC2",
					},
				},
				{
					Name:       "LAN1",
					Region:     "RN1",
					RegionCode: "RC1",
					Codes: map[string]string{
						"OAC1": "OAC1",
					},
				},
			},
		},
		{
			name:          "no matching queries",
			query:         []string{"foo", "bar"},
			expectedNames: []*payloads.AreaResp{},
		},
	}

	// run tests
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			matchingAreas := getAllMatchingAreas(tt.query, mockDB)
			if len(matchingAreas) != len(tt.expectedNames) {
				t.Errorf("expected %d matching areas, got %d", len(tt.expectedNames), len(matchingAreas))
				return
			}
			for i, areaResp := range matchingAreas {
				if areaResp.Name != tt.expectedNames[i].Name ||
					areaResp.Region != tt.expectedNames[i].Region ||
					areaResp.RegionCode != tt.expectedNames[i].RegionCode {
					t.Errorf("expected area with name %s, got %s", tt.expectedNames[i], areaResp)
				}
			}
		})
	}
}
func mockInds() []*db.Industry {
	industries := []*db.Industry{
		{Code: "IND1", Name: "Industry 1"},
		{Code: "IND2", Name: "Industry 2"},
		{Code: "IND3", Name: "Industry 3"},
	}

	return industries
}

func mockAreas() []*db.Area {
	areas := []*db.Area{
		{
			RegionCode:         "RC1",
			OutputAreaCode:     "OAC1",
			LocalAuthorityCode: "LAC1",
			LAName:             "LAN1",
			RegionName:         "RN1",
		},
		{
			RegionCode:         "RC2",
			OutputAreaCode:     "OAC2",
			LocalAuthorityCode: "LAC2",
			LAName:             "LAN2",
			RegionName:         "RN2",
		},
		{
			RegionCode:         "RC3",
			OutputAreaCode:     "OAC3",
			LocalAuthorityCode: "LAC3",
			LAName:             "LAN3",
			RegionName:         "RN3",
		},
	}

	return areas
}

func mockDb() *db.ScrubberDB {
	areaData := mockAreas()
	industryData := mockInds()

	areasMap := prefixmap.New()
	for _, area := range areaData {
		areasMap.Insert(area.OutputAreaCode, area)
	}

	industryMap := prefixmap.New()
	for _, industry := range industryData {
		industryMap.Insert(industry.Code, industry)
	}

	return &db.ScrubberDB{
		AreasPFM:      areasMap,
		IndustriesPFM: industryMap,
	}
}

func skipUnitTests(t *testing.T) {
	if os.Getenv("UNIT") != "" {
		t.Skip("Skipping Unit tests in CI environment")
	}
}
