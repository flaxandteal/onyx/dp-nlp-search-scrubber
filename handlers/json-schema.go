package handlers

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/ONSdigital/log.go/v2/log"
	"github.com/invopop/jsonschema"
	"gitlab.com/flaxandteal/onyx/dp-nlp-search-scrubber/payloads"
)

func ScrubberSchemaHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Namespace = "Scrubber Schema Handler"
		ctx := context.Background()
		w.WriteHeader(http.StatusOK)
		k, err := scrubberSchemaGenerator()
		if err != nil {
			log.Error(ctx, "Error unmarshaling json in ScrubberSchemaHandler: %v", err)
		}
		w.Write(k)
	}
}

func scrubberSchemaGenerator() ([]byte, error) {
	schemaJSON := jsonschema.Reflect(payloads.ScrubberResp{})
	marshalSchema, err := json.MarshalIndent(schemaJSON, "", "  ")
	if err != nil {
		return []byte("Can't marshal the schema."), err
	}

	return marshalSchema, nil
}
