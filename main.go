package main

import (
	"context"
	"fmt"
	"net/http"

	"github.com/ONSdigital/log.go/v2/log"
	"github.com/gorilla/mux"
	"gitlab.com/flaxandteal/onyx/dp-nlp-search-scrubber/config"
	"gitlab.com/flaxandteal/onyx/dp-nlp-search-scrubber/db"
	"gitlab.com/flaxandteal/onyx/dp-nlp-search-scrubber/handlers"
)

func main() {
	log.Namespace = "dp-nlp-search-scrubber"

	ctx := context.Background()

	cfg := config.GetCfg(ctx)

	cfg.Info(ctx)

	scrubberDB := db.LoadCsvData(cfg)
	log.Info(ctx, "Successfully loaded scrubber db")

	r := mux.NewRouter()
	routes(r, &scrubberDB)
	srv := &http.Server{
		Addr:    cfg.Addr,
		Handler: r,
	}

	log.Info(ctx, fmt.Sprintf("Starting server on %s", cfg.Addr))
	log.Fatal(ctx, "Server stopped: ", srv.ListenAndServe())
}

func routes(r *mux.Router, scrubberDB *db.ScrubberDB) {

	r.Path("/scrubber/search").
		Queries("q", "{q}").
		HandlerFunc(handlers.PrefixSearchHandler(scrubberDB)).
		Name("Show")

	r.Path("/health").
		HandlerFunc(handlers.HealthCheck()).
		Name("Health")

	r.Path("/json-schema").
		HandlerFunc(handlers.ScrubberSchemaHandler()).
		Name("json-schema")
}
