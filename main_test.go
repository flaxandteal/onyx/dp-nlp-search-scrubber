package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/invopop/jsonschema"
	"gitlab.com/flaxandteal/onyx/dp-nlp-search-scrubber/config"
	"gitlab.com/flaxandteal/onyx/dp-nlp-search-scrubber/db"
	"gitlab.com/flaxandteal/onyx/dp-nlp-search-scrubber/handlers"
	"gitlab.com/flaxandteal/onyx/dp-nlp-search-scrubber/payloads"
)

func TestScrubberJsonSchema(t *testing.T) {
	// Split API tests from Unit tests
	skipApiTests(t)
	// Create a request to pass to our handler.
	req, err := http.NewRequest("GET", "/scrubber/json-schema", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(handlers.ScrubberSchemaHandler())

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	schemJSON := jsonschema.Reflect(payloads.ScrubberResp{})
	expected, _ := json.MarshalIndent(schemJSON, "", "  ")

	if rr.Body.String() != string(expected) {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestHealthCheck(t *testing.T) {
	// Split API tests from Unit tests
	skipApiTests(t)

	// Create a request to pass to our handler.
	req, err := http.NewRequest("GET", "/health", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(handlers.HealthCheck())

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expected := `OK`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestPrefixSearchHandler(t *testing.T) {
	// Split API tests from Unit tests
	skipApiTests(t)
	// Create a test database
	db := setupTestDB()

	// Create a request to be sent to the API
	req, err := http.NewRequest("GET", "/scrubber/search?query=test", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Create a response recorder to record the API response
	rr := httptest.NewRecorder()
	hf := http.HandlerFunc(handlers.PrefixSearchHandler(db))

	// Serve the request to the API
	hf.ServeHTTP(rr, req)

	// Check the status code is what we expect
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Unmarshals the response to get the exact time needed in expected response
	var scrubberResp payloads.ScrubberResp
	err = json.Unmarshal(rr.Body.Bytes(), &scrubberResp)
	if err != nil {
		t.Fatalf("Error unmarshaling JSON: %v", err)
	}

	// Check the response body is what we expect
	expected := fmt.Sprintf(`{"time":"%v","query":"","results":{"areas":null,"industries":null}}`, scrubberResp.Time)
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func setupTestDB() *db.ScrubberDB {
	// Creates a config required by LoadCsvData
	cfg := config.Config{
		Addr:         ":3002",
		AreaFile:     "data/2011 OAC Clusters and Names csv v2.csv",
		IndustryFile: "data/SIC07_CH_condensed_list_en.csv",
	}
	// Loads TestDb
	db := db.LoadCsvData(&cfg)
	return &db
}

func TestScrubberHealthCheck(t *testing.T) {
	skipCI(t)
	// Create a http get request to scrubber health endpoint.
	res, err := http.Get("http://localhost:3002/health")
	if err != nil {
		t.Errorf("Health check request failed: %s", err)
	}
	// Close body last
	defer res.Body.Close()
	if res.StatusCode != 200 {
		t.Errorf("Health check failed, expected 200 OK but got %d", res.StatusCode)
	}
}

func TestScrubberSearch(t *testing.T) {
	skipCI(t)
	res, err := http.Get("http://localhost:5000/scrubber/search?q=dentists")
	if err != nil {
		t.Errorf("Scrubber search request failed: %s", err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		t.Errorf("Scrubber search failed, expected 200 OK but got %d", res.StatusCode)
	}
}

func TestScrubberSearchSchema(t *testing.T) {
	skipCI(t)
	res, err := http.Get("http://localhost:5000/json-schema")
	if err != nil {
		t.Errorf("Scrubber search schema request failed: %s", err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		t.Errorf("Scrubber search schema failed, expected 200 OK but got %d", res.StatusCode)
	}
}

// Skip these test in ci for now, because they require scrubber to be run on localhost:3002
func skipCI(t *testing.T) {
	if os.Getenv("CI") != "" {
		t.Skip("Skipping testing in CI environment")
	}
}

func skipApiTests(t *testing.T) {
	if os.Getenv("API") != "" {
		t.Skip("Skipping Api tests in CI environment")
	}
}
