module gitlab.com/flaxandteal/onyx/dp-nlp-search-scrubber

go 1.19

require (
	github.com/ONSdigital/log.go/v2 v2.3.0
	github.com/alediaferia/prefixmap v1.0.1
	github.com/gocarina/gocsv v0.0.0-20230123225133-763e25b40669
	github.com/gorilla/mux v1.8.0
	github.com/invopop/jsonschema v0.7.0
	github.com/joho/godotenv v1.5.1
	github.com/kelseyhightower/envconfig v1.4.0
)

require (
	github.com/ONSdigital/dp-api-clients-go v1.43.0 // indirect
	github.com/ONSdigital/dp-api-clients-go/v2 v2.187.0 // indirect
	github.com/ONSdigital/dp-net v1.5.0 // indirect
	github.com/ONSdigital/dp-net/v2 v2.6.0 // indirect
	github.com/fatih/color v1.13.0 // indirect
	github.com/hokaccha/go-prettyjson v0.0.0-20211117102719-0474bc63780f // indirect
	github.com/iancoleman/orderedmap v0.0.0-20190318233801-ac98e3ecb4b0 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	golang.org/x/sys v0.0.0-20220811171246-fbc7d0a398ab // indirect
	gopkg.in/alediaferia/stackgo.v1 v1.1.1 // indirect
)
