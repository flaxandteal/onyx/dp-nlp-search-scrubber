package db

import (
	"os"
	"testing"

	"gitlab.com/flaxandteal/onyx/dp-nlp-search-scrubber/config"
)

func TestGetArea(t *testing.T) {
	// Split API tests from Unit tests
	skipUnitTests(t)

	// create a test file with test data and write test data to the test file
	ar := createAreaDb(t)

	// check if the function returns the expected result
	if len(ar) != 2 {
		t.Errorf("Expected 2 areas, but got %d", len(ar))
	}

	if ar[0].OutputAreaCode != "Test Output Area Code1" || ar[0].LocalAuthorityCode != "Test LAC1" || ar[0].LAName != "Test LAN1" || ar[0].RegionCode != "Test RC1" || ar[0].RegionName != "Test RN 1" {
		t.Errorf("Expected OutputAreaCode: 'Test Output Area Code1', LocalAuthorityCode: 'Test LAC1', LAName: 'Test LAN1', RegionCode: 'Test RC1', RegionName: 'Test RN 1', but got OutputAreaCode: '%s', LocalAuthorityCode: '%s', LAName: '%s', RegionCode: '%s', RegionName: '%s'", ar[0].OutputAreaCode, ar[0].LocalAuthorityCode, ar[0].LAName, ar[0].RegionCode, ar[0].RegionName)
	}

	if ar[1].OutputAreaCode != "Test Output Area Code2" || ar[1].LocalAuthorityCode != "Test LAC2" || ar[1].LAName != "Test LAN2" || ar[1].RegionCode != "Test RC2" || ar[1].RegionName != "Test RN 2" {
		t.Errorf("Expected OutputAreaCode: 'Test Output Area Code2', LocalAuthorityCode: 'Test LAC2', LAName: 'Test LAN2', RegionCode: 'Test RC2', RegionName: 'Test RN 2', but got OutputAreaCode: '%s', LocalAuthorityCode: '%s', LAName: '%s', RegionCode: '%s', RegionName: '%s'", ar[0].OutputAreaCode, ar[0].LocalAuthorityCode, ar[0].LAName, ar[0].RegionCode, ar[0].RegionName)
	}
}

func createAreaDb(t *testing.T) []*Area {
	testFile, err := os.Create("test.csv")
	if err != nil {
		t.Fatalf("Failed to create test file: %v", err)
	}
	defer testFile.Close()
	defer os.Remove("test.csv")

	_, err = testFile.WriteString("Output Area Code,Local Authority Code,Local Authority Name,Region/Country Code,Region/Country Name\nTest Output Area Code1,Test LAC1,Test LAN1,Test RC1,Test RN 1\nTest Output Area Code2,Test LAC2,Test LAN2,Test RC2,Test RN 2\n")
	if err != nil {
		t.Fatalf("Failed to write test data: %v", err)
	}

	cfg := config.Config{
		AreaFile: "test.csv",
	}

	ar, err := getArea(&cfg)
	if err != nil {
		t.Fatalf("Failed to write test data: %v", err)
	}
	return ar
}
