package db

import (
	"os"
	"testing"

	"gitlab.com/flaxandteal/onyx/dp-nlp-search-scrubber/config"
)

func TestGetIndustry(t *testing.T) {
	// Split API tests from Unit tests
	skipUnitTests(t)

	// create a test file with test data and write test data to the test file
	ir := createIndustryDb(t)

	// check if the function returns the expected result
	if len(ir) != 2 {
		t.Errorf("Expected 2 areas, but got %d", len(ir))
	}

	if ir[0].Code != "TestCode1" || ir[0].Name != "TestName1" {
		t.Errorf("Expected Sic Code: 'TestCode1', Descr: 'TestName1', but got Sic Code: '%s', Descr: '%s'", ir[0].Code, ir[0].Name)
	}

	if ir[1].Code != "TestCode2" || ir[1].Name != "TestName2" {
		t.Errorf("Expected Sic Code: 'TestCode2', Descr: 'TestName2', but got Sic Code: '%s', Descr: '%s'", ir[0].Code, ir[0].Name)
	}
}

func createIndustryDb(t *testing.T) []*Industry {
	testFile, err := os.Create("test.csv")
	if err != nil {
		t.Fatalf("Failed to create test file: %v", err)
	}
	defer testFile.Close()
	defer os.Remove("test.csv")

	_, err = testFile.WriteString("SIC Code,Description\nTestCode1,TestName1\nTestCode2,TestName2\n")
	if err != nil {
		t.Fatalf("Failed to write test data: %v", err)
	}

	cfg := config.Config{
		IndustryFile: "test.csv",
	}

	ir, err := getIndustry(&cfg)
	if err != nil {
		t.Fatalf("Failed to create test file: %v", err)
	}
	return ir
}
func skipUnitTests(t *testing.T) {
	if os.Getenv("UNIT") != "" {
		t.Skip("Skipping Unit tests in CI environment")
	}
}
