package db

import (
	"testing"

	"gitlab.com/flaxandteal/onyx/dp-nlp-search-scrubber/config"
)

func TestGetLoadCsvData(t *testing.T) {
	// Split API tests from Unit tests
	skipUnitTests(t)

	cfg := config.Config{
		AreaFile:     "../data/2011 OAC Clusters and Names csv v2.csv",
		IndustryFile: "../data/SIC07_CH_condensed_list_en.csv",
	}
	sr := LoadCsvData(&cfg)
	// check if the function returns the expected result
	if sr.AreasPFM == nil || sr.IndustriesPFM == nil {
		t.Errorf("Expected areas and industry PFM exists, but got %v", sr.AreasPFM)
	}
	matchingRecords := sr.AreasPFM.GetByPrefix("E00000001")
	area := matchingRecords[0].(*Area)
	if area.RegionCode != "E12000007" || area.RegionName != "London" {
		t.Errorf("Expected RegionCode: 'E12000007', RegionName: 'London', but got RegionCode: '%s', RegionName: '%s'", area.RegionCode, area.RegionName)
	}
}
