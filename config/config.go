package config

import (
	"context"
	"fmt"

	"github.com/ONSdigital/log.go/v2/log"
	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	Addr         string
	AreaFile     string
	IndustryFile string
}

// Get configures the application and returns the Config
func GetCfg(ctx context.Context) *Config {
	cfg := &Config{}

	// default arg for .Load() is .env
	if err := godotenv.Load(); err != nil {
		log.Error(ctx, "error loading .env file: %s", err)
	}

	if err := envconfig.Process("APP", cfg); err != nil {
		log.Error(ctx, "error processing env variables", err)
	}

	if cfg.Addr != "" {
		return cfg
	}

	cfg = &Config{
		Addr:         ":3002",
		AreaFile:     "data/2011 OAC Clusters and Names csv v2.csv",
		IndustryFile: "data/SIC07_CH_condensed_list_en.csv",
	}

	return cfg
}

func (c *Config) Info(ctx context.Context) {
	log.Info(ctx, fmt.Sprintf("AreaFile location: %v", c.AreaFile))
	log.Info(ctx, fmt.Sprintf("IndustryFile location: %v", c.IndustryFile))
	log.Info(ctx, fmt.Sprintf("/search searches for area and industry codes"))
	log.Info(ctx, fmt.Sprintf("/health is a simple healthcheck"))
	log.Info(ctx, fmt.Sprintf("/json-schema gets a jsonSchema of the response"))
	log.Info(ctx, fmt.Sprintf("Started server on port: %v", c.Addr))
}
